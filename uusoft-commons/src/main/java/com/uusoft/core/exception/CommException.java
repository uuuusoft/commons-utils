package com.uusoft.core.exception;

/**
 * 通用异常类
 * @author shisheng.lian
 * @date 2014-7-16
 */
public class CommException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CommException() {
		super();
	}

	public CommException(String message) {
		super(message);
	}

	public CommException(Throwable cause) {
		super(cause);
	}

	public CommException(String message, Throwable cause) {
		super(message, cause);
	}

}
