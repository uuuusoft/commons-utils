package com.uusoft.core.utils;

/**
 * 断言工具类
 * @author shisheng.lian
 *
 */
public class Assert {
	/**
	 * 判断obj是否为空
	 * 
	 * @param obj
	 * 
	 * @param message
	 */
	public static void nullObject(Object obj, String message) {
		if (null == obj) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * 判断assString是否为空
	 * 
	 * @para assString
	 * @param message
	 */
	public static void nullString(String assString, String message) {
		if (null == assString || "".equals(assString)) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * 检查指定字符串是否符合指定长度
	 * 
	 * @param assString
	 * @param length
	 * @param messageForNUll
	 * @param messageForLength
	 * 
	 */
	public static void limitLength(String assString, int length,
			String messageForNull, String messageForLength) {
		Assert.nullString(assString, messageForNull);
		if (StringUtils.trim(assString).length() != length) {
			throw new IllegalArgumentException(messageForNull);
		}
	}

	/**
	 * 判断int数据是否是正整数
	 * 
	 * @param assInt
	 * @param message
	 * 
	 */
	public static void nullInt(int assInt, String message) {
		if (assInt <= 0) {
			throw new IllegalArgumentException(message);
		}
	}
}
