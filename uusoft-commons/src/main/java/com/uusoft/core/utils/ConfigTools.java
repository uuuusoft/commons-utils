package com.uusoft.core.utils;

import java.security.NoSuchAlgorithmException;

import com.uusoft.core.utils.security.EncryptUtils;


/**
 * 客户端配置工具
 * @author shisheng.lian
 *
 */
public class ConfigTools {

	public static void main(String[] args) {
		try {
			if (null == args || 0 == args.length) {
				System.err.println("输入参数为空！");
				return;
			}
			
			String operate = args[0];
			if ("security.genKey".equals(operate)) { //生成密钥
				genKey(args);
			} else if ("security.encrypt".equals(operate)) {
				encrypt(args);
			} else if ("security.decrypt".equals(operate)) {
				decrypt(args);
			} else if ("?".equals(operate)) {
				printHelp();
			} else {
				System.err.println("操作名参数错误！");
				printHelp();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 解密数据
	 * @param args
	 * @throws Exception
	 */
	private static void decrypt(String[] args) throws Exception {
		if (args.length < 2 || isNull(args[1])) {
			System.err.println("要解密的字符串为空！");
		} else {
			String dataStr = args[1]; //要解密的字符串
			String publicKey = null;
			if (args.length > 2) {
				publicKey = args[2]; //解密密钥（公钥）
			}
			String decryptedStr = EncryptUtils.decrypt(publicKey, dataStr);
			System.out.println("解密后数据：");
			System.out.println(decryptedStr);
		}
	}

	/**
	 * 加密数据
	 * @param args
	 * @throws Exception
	 */
	private static void encrypt(String[] args) throws Exception {
		if (args.length < 2 || isNull(args[1])) {
			System.err.println("要加密的字符串为空！");
		} else {
			String dataStr = args[1]; //要加密的字符串
			String privateKey = null;
			if (args.length > 2) {
				privateKey = args[2]; //加密密钥
			}
			String encryptedStr = EncryptUtils.encrypt(privateKey, dataStr);
			System.out.println("加密后数据：");
			System.out.println(encryptedStr);
		}
	}

	/**
	 * 生成密钥
	 * @param args
	 * @throws NoSuchAlgorithmException
	 */
	private static void genKey(String[] args) throws NoSuchAlgorithmException {
		int keySize = 1024;
		if (args.length > 1) {
			String keySizeStr = args[1];
			if (!isNull(keySizeStr)) {
				keySize = Integer.parseInt(keySizeStr);
			}
		}
		String[] generateKey = EncryptUtils.genKeyPair(keySize);
		System.out.println("---------下面为系统自动生成的公钥及私钥，请使用私钥对您的数据进行加密处理，公钥则填写到配置文件中供系统解密使用-------------");
		System.out.println("私钥：\n" + generateKey[0]);
		System.out.println();
		System.out.println("公钥：\n" + generateKey[1]);
	}
	
	/**
	 * 打印帮助
	 */
	private static void printHelp() {
		System.err.println("security.genKey：\n生成加密解密所需要的私钥及公钥密钥串——>第一个参数为密钥位数，该值应该为正整数值，可以为空 。");
		System.err.println("security.encrypt：\n对字符串进行加密处理——>第一个参数为要加密的字符串，该值不能为空；第二个参数为加密时所需要的私钥，该参数可以为空，为空是系统使用默认私钥进行加密。");
		System.err.println("security.decrypt：\n对已加密串进行解密——>第一个参数为要解密的字符串，该值不能为空；第二个参数为与加密私钥对应的公钥，该参数可以为空，为空是系统使用默认公钥进行解密。");
		System.err.println("有关更详细的说明请参考sif使用手册");
	}
	
	private static boolean isNull(String str) {
		return null == str || "".equals(str);
	}

}
