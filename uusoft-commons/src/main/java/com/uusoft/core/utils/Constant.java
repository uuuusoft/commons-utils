package com.uusoft.core.utils;

/**
 * 配置文件名称常量
 * 
 * @author shisheng.lian
 * 
 */
public interface Constant {

	/**
	 * 系统敏感数据解密密钥(公钥)
	 */
	public static final String ENCRYPT_KEY = "us.encrypt.key";

	/**
	 * 系统敏感数据加密前缀标识
	 */
	public static final String ENCRYPT_PREFIX = "us.encrypt:";
	
	/**
	 * 缓存提供者，配置文件字段名称
	 */
	public static final String CACHE_PROVIDER = "us.cache.provider";
	
	/**
	 * 缓存配置，配置文件字段名称
	 */
	public static final String CACHE_CONF_FILE = "us.cache.conf.file";
	
	/**
	 * 默认缓存配置文件
	 */
	public static final String DEFAULT_CACHE_CONF_FILE = "ehcache.xml";
	
	/**
	 * 数据源配置名称前缀
	 */
	public static final String DATASOURCE_PREFIX = "us.datasource";

	/**
	 * 数据源提供类（com.thitech.sif.utils.jdbc.DataSourceAdapter的实现类）,类名需设置为全限定名
	 */
	public static final String DATASOURCE_ADAPTER = DATASOURCE_PREFIX + ".adapter";

	/**
	 * 数据库类型，支持的类型有：mysql、oracle、db2、mssql
	 */
	public static final String DATASOURCE_ADAPTER_DATABASE = DATASOURCE_PREFIX + ".adapter.database";

	/**
	 * 数据库表前缀
	 */
	public static final String DATASOURCE_ADAPTER_TABLEPREFIX = DATASOURCE_PREFIX + ".adapter.tableprefix";

	/**
	 * 数据源名称
	 */
	public static final String DATASOURCE_ADAPTER_DS = DATASOURCE_PREFIX + ".adapter.DS";

}
