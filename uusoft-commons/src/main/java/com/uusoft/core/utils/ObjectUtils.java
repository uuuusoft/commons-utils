package com.uusoft.core.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * object utils
 * 
 * @author shisheng.lian
 * 
 */
public class ObjectUtils {
	/**
	 * 把object转换成stirng
	 */
	public static String obj2String(Object obj) {
		String result = "";
		if (null != obj) {
			if (obj instanceof String) {
				result = (String) obj;
			}
		}
		return result;
	}

	/**
	 * 把object转换成boolean
	 */
	public static boolean obj2Boolean(Object obj) {
		boolean result = false;
		if (null != obj) {
			if (obj instanceof Boolean) {
				result = (Boolean) obj;
			}
		}
		return result;
	}

	/**
	 * 把对象转换成指定数据类型
	 */
	@SuppressWarnings("unchecked")
	public static <T> T obj2t(Object obj, Class<T> classzz) {
		T t = null;
		if (null != obj) {
			t = (T) obj;
		}
		return t;
	}

	/**
	 * 把object数组转换成map
	 * 
	 */
	public static Map<String, Object> objFill2Map(Object... objs) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (null != objs && 0 != objs.length) {
			for (int i = 0; i < objs.length; i++) {
				Object obj = objs[i];
				if (null != obj) {
					map.put(obj.getClass().getSimpleName(), obj);
				}
			}
		}
		return map;
	}
}
