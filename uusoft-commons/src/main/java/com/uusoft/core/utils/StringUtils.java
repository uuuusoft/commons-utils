package com.uusoft.core.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.uusoft.core.utils.log.LogFactory;
import com.uusoft.core.utils.log.USoftLogger;

/**
 * 字符串相关工具
 * 
 * @author shisheng.lian
 * 
 */
public class StringUtils {
	
	private static USoftLogger log = LogFactory.getLogger(StringUtils.class);
	
	/**
	 * 去掉字符串两端空格
	 * 
	 * @param str
	 */
	public static String trim(String str) {
		return (null != str && !"".equals(str)) ? str.trim() : "";
	}

	public static String trimNull(String source) {
		return "".equals(source) ? null : source;
	}
	
	/**
	 * 判断字符串是否为空
	 * @param str
	 * @return true is null
	 */
	public static boolean isNull(String str) {
		return null == str || "".equals(str);
	}

	/**
	 * 
	 * 把字符串转换成byte数组
	 * 
	 * @param str
	 */
	public static byte[] string2Bytes(String str) {
		byte[] b = null;

		try {
			if (!"".equals(str) && null != str) {
				b = str.getBytes("utf-8");
			}
		} catch (Exception e) {
			log.error("Dump String to bytes exception", e);
		}
		return b;
	}

	public static String date2String(Date d, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(d);
	}

	public static String date2String(Date d) {
		return date2String(d, "yyyy-MM-dd HH:mm:ss.SSS");
	}
}
