package com.uusoft.core.utils.cache;
/**
 * 缓存异常
 * @author shisheng.lian
 */
public class CacheException extends RuntimeException {
	private static final long serialVersionUID = 6086949290707614691L;

	public CacheException(String s) {
        super(s);
    }
 
    public CacheException(String s, Throwable e) {
        super(s, e);
    }
 
    public CacheException(Throwable e) {
        super(e);
    }
     
}