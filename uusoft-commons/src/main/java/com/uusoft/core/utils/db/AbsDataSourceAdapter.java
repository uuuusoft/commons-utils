package com.uusoft.core.utils.db;

import java.util.Properties;

import javax.sql.DataSource;

import com.uusoft.core.utils.Assert;
import com.uusoft.core.utils.Constant;
import com.uusoft.core.utils.PropertiesParser;

public abstract class AbsDataSourceAdapter implements DataSourceAdapter {
	
	private static DataSource ds = null;
	private static Object mutext = new Object();

	public static void setDs(DataSource ds) {
		AbsDataSourceAdapter.ds = ds;
	}

	public DataSource getDataSource() {
		if (null == ds) {
			synchronized (mutext) {
				init();
			}
		}
		return ds;
	}
	
	/**
	 * 根据DS名称获取数据源连接数据库所需要的配置信息
	 * @return
	 */
	public Properties getDSPropertiesGroup() {
		PropertiesParser baseProperties = new PropertiesParser();
		String dsName = baseProperties.getStringProperty(Constant.DATASOURCE_ADAPTER_DS);
		Assert.nullString(dsName, "从配置文件中获取数据源名称为空！");
		
		Properties propertiesGroup = baseProperties.getPropertyGroup(Constant.DATASOURCE_PREFIX + "." + dsName, true);
		return propertiesGroup;
	}
	
	public abstract void init();
}
