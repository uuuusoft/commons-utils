package com.uusoft.core.utils.db;

import javax.sql.DataSource;

/**
 * 本框架所需要使用的DataSource接口，由框架提供一套默认的实现，使用时也可实现该接口自己提供DataSource供框架使用
 * 
 * @author shisheng.lian
 * @date 2014-08-13
 * 
 */
public interface DataSourceAdapter {

	/**
	 * 获取数据源
	 * @return
	 */
	public DataSource getDataSource();
}
