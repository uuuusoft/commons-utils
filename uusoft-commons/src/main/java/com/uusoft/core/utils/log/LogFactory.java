package com.uusoft.core.utils.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * sif日志工厂
 * 
 * @author shisheng.lian
 * @date 2014-08-08
 * 
 */
public class LogFactory {
	public static USoftLogger getLogger(Class<?> classzz) {
		Logger logger = LoggerFactory.getLogger(classzz);
		return new USoftLogger(logger);
	}

	public static USoftLogger getLogger(String name) {
		Logger logger = LoggerFactory.getLogger(name);
		return new USoftLogger(logger);
	}
}
