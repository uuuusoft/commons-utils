package com.uusoft.core.utils.log;

import org.slf4j.Logger;

/**
 * uusoft日志工具类
 * 
 * @author shisheng.lian
 * @date 2014-08-08
 * 
 */
public class USoftLogger {
	private Logger logger;

	protected USoftLogger(Logger logger) {
		this.logger = logger;
	}

	public void debug(String message) {
		if (logger.isDebugEnabled()) {
			logger.debug(message);
		}
	}

	public void debug(String message, Throwable e) {
		if (logger.isDebugEnabled()) {
			logger.debug(message, e);
		}
	}

	public void error(String message) {
		if (logger.isErrorEnabled()) {
			logger.error(message);
		}
	}

	public void error(String message, Throwable e) {
		if (logger.isErrorEnabled()) {
			logger.error(message, e);
		}
	}

	public void info(String message) {
		if (logger.isInfoEnabled()) {
			logger.info(message);
		}
	}

	public void info(String message, Throwable e) {
		if (logger.isInfoEnabled()) {
			logger.info(message, e);
		}
	}

	public void trace(String message) {
		if (logger.isTraceEnabled()) {
			logger.trace(message);
		}
	}

	public void trace(String message, Throwable e) {
		if (logger.isTraceEnabled()) {
			logger.trace(message, e);
		}
	}

	public void warn(String message) {
		if (logger.isWarnEnabled()) {
			logger.warn(message);
		}
	}

	public void warn(String message, Throwable e) {
		if (logger.isWarnEnabled()) {
			logger.warn(message, e);
		}
	}
}
