package com.uusoft.core.utils.xml;

import com.uusoft.core.exception.CommException;

/**
 * XML校验工具
 * @author shisheng.lian
 *
 */
public interface XmlValidate {
	/**
	 * 校验指定的XML文档
	 */
	public void validateXML() throws CommException;
}
