package com.uusoft;

import java.util.Locale;

import org.junit.Test;

import com.uusoft.core.utils.i18n.Messages;

public class MessageTest {

	@Test
	public void getMessage() {
		Locale.setDefault(Locale.US);
		String message = Messages.getMessage("sif.service.invoke.exception");
		System.out.println(message);
	}
}
